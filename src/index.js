import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {createStore,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import {
    BrowserRouter,
    Route,
    Link,
    Redirect,
    Switch
} from 'react-router-dom'; 
import {counter} from './index.redux';
import {Provider} from 'react-redux';


const reduxDevtools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = createStore(counter,compose(
        applyMiddleware(thunk),
        reduxDevtools
    ));

function User(){
    return <h1>用户列表</h1>
}
function Product(){
    return <h1>商品列表</h1>
}

class Test extends React.Component{
    render(){
      
        return <h2>测试组件{this.props.match.params.location}</h2>
    }
}

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
        <div>
            <ul>
                <li>
                    <Link to='/'>首页</Link>
                </li>
                <li>
                    <Link to='/user'>个人中心</Link>
                </li>

                <li>
                    <Link to='/product'>商品列表</Link>
                </li>
            </ul>
            {/* <Redirect to='/user'></Redirect> */}
            <Switch>
                <Route path='/' exact component={App}></Route>  
                <Route path='/:location'  component={Test}></Route>  
                <Route path='/user' component={User}></Route>
                <Route path='/product' component={Product}></Route>
            </Switch>
        </div>  
        </BrowserRouter>
    </Provider>,  
     document.getElementById('root')
)
registerServiceWorker();



//1、exact 完全匹配
//2、Redirect 重定向
//3、Switch 只渲染命中的第一个路由
//4、redux开发工具   https://github.com/zalmoxisus/redux-devtools-extension#usage 
