import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux'
import {add,dell,addGunAsync} from './index.redux';

//redux源码分析//https://juejin.im/post/5aa232f651882518803884dd

// const mapStatetoProps=(state)=>{
// 	return {num:state}
// }
// const actionCreators = {add,dell,addGunAsync};
// App = connect(mapStatetoProps,actionCreators)(App);

@connect(
	//属性放到props里
	state=>({num:state}),
	//方法放到props里
	{add,dell,addGunAsync}
)

class App extends Component{


	render() {
		return (
			<div className="App">
				<p>我是测试数据</p>
				<p>{this.props.num}</p>
				<button onClick={this.props.add}>添加一条</button>
				<button onClick={this.props.dell}>删除一条</button>
				<button onClick={this.props.addGunAsync}>等待</button>
			</div>
		);
	}
}

export default App;
