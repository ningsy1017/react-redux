
const ADD_GUN = '加';
const DEL_GUN ="减";
//reducer
export  function counter (state=0,action) {
    switch (action.type){
        case '加':
            return state+1
        case '减':
            return state-1
        default:
            return 10 
    }
}

export function add(){
    return {type:ADD_GUN}
}
export function dell(){
    return {type:DEL_GUN}
}
export function addGunAsync(){
    return dispatch=>{
        setTimeout(()=>{
            dispatch(add())
        },2000)
    }
}